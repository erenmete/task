# Prerequisites(Ubuntu)
*  Maven 3+
*  jdk-8+

# Technologies
*  spring-boot-starter-parent 2.1.5 
*  junit-4.12
 
# Build Project
## Command-line Instructions(Ubuntu)
    
*  Run the following command in the the folder where pom.xml is, to build the project. (Runs all unit tests)
  
    mvn package