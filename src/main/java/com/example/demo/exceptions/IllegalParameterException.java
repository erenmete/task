package com.example.demo.exceptions;

public class IllegalParameterException extends RuntimeException {
    public IllegalParameterException(String errorMessage) {
        super(errorMessage);
    }
}
