package com.example.demo.exceptions;

public class NotSupportedException extends RuntimeException {
    public NotSupportedException(String errorMessage) {
        super(errorMessage);
    }
}