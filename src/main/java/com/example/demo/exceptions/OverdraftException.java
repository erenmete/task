package com.example.demo.exceptions;

public class OverdraftException extends RuntimeException{
    public OverdraftException(String errorMessage) {
        super(errorMessage);
    }
}