package com.example.demo.model;

import java.math.BigDecimal;

public abstract class Account {

    private Owner owner;
    private BigDecimal balance;


    public Account(Owner owner, BigDecimal balance){
        this.owner= owner;
        this.balance = balance;
    }


    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }


    public abstract BigDecimal getOverdraft();
    public abstract boolean canPayInterest();
    public abstract boolean canTransfer();

}
