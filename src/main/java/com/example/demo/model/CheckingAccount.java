package com.example.demo.model;

import java.math.BigDecimal;

public class CheckingAccount extends Account{
    private BigDecimal overdraftLimit;

    public CheckingAccount(Owner owner, BigDecimal balance, BigDecimal overdraftLimit) {
        super(owner, balance);
        this.overdraftLimit = overdraftLimit;
    }

    public BigDecimal getOverdraftLimit() {
        return overdraftLimit;
    }

    public void setOverdraftLimit(BigDecimal overdraftLimit) {
        this.overdraftLimit = overdraftLimit;
    }



    @Override
    public BigDecimal getOverdraft() {
        return overdraftLimit;
    }

    @Override
    public boolean canPayInterest() {
        return false;
    }

    @Override
    public boolean canTransfer() {
        return true;
    }
}
