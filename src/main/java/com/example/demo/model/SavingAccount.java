package com.example.demo.model;

import java.math.BigDecimal;

public class SavingAccount extends Account{
    private double interestRate;

    public SavingAccount(Owner owner, BigDecimal balance, double interestRate) {
        super(owner, balance);
        this.interestRate = interestRate;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }



    @Override
    public BigDecimal getOverdraft() {
        return BigDecimal.ZERO;
    }

    @Override
    public boolean canPayInterest() {
        return true;
    }

    @Override
    public boolean canTransfer() {
        return false;
    }
}
