package com.example.demo.service;

import com.example.demo.exceptions.IllegalParameterException;
import com.example.demo.exceptions.NotSupportedException;
import com.example.demo.exceptions.OverdraftException;
import com.example.demo.model.Account;
import com.example.demo.model.SavingAccount;
import org.springframework.stereotype.Service;


import java.math.BigDecimal;

@Service
public class AccountService {

    public static String NOT_SUPPORTED_MSG = "Operation is not supported for this account type";
    public static String OVERDRAFT_ERROR_MSG  = "Not enough balance";
    public static String AMOUNT_ERROR_MSG = "Illegal parameter";

    public void deposit(Account account, BigDecimal depositAmount) throws IllegalParameterException {
        if(depositAmount.compareTo(BigDecimal.ZERO) < 0){
            throw new IllegalParameterException(AMOUNT_ERROR_MSG);
        }
        BigDecimal balance = account.getBalance();
        balance = balance.add(depositAmount);
        account.setBalance(balance);
    }

    public void transfer(Account sourceAccount, Account targetAccount, BigDecimal amount) throws NotSupportedException,
            OverdraftException, IllegalParameterException{
        if( ! (sourceAccount.canTransfer() && targetAccount.canTransfer()) ){
            throw new NotSupportedException(NOT_SUPPORTED_MSG);
        } else {
            boolean withdrawSuccess = false;
            boolean depositSuccess = false;
            try{
                withdraw(sourceAccount, amount);
                withdrawSuccess = true;
                deposit(targetAccount, amount);
                depositSuccess = true;
            }catch (IllegalParameterException ipe){
                throw ipe;
            }catch (OverdraftException oe){
                throw oe;
            }catch (Exception e){
                if( withdrawSuccess && !depositSuccess ){
                    revertTransfer(sourceAccount, amount);
                }
            }
        }
    }

    public void withdraw(Account account, BigDecimal withdrawAmount) throws OverdraftException, IllegalParameterException {
        if(withdrawAmount.compareTo(BigDecimal.ZERO) < 0){
            throw new IllegalParameterException(AMOUNT_ERROR_MSG);
        }

        BigDecimal currentBalance = account.getBalance();

        if( currentBalance.subtract(withdrawAmount).compareTo(account.getOverdraft().negate()) < 0 ){
            throw new OverdraftException(OVERDRAFT_ERROR_MSG);
        } else {
            currentBalance = currentBalance.subtract(withdrawAmount);
            account.setBalance(currentBalance);
        }
    }

    public void revertTransfer(Account account, BigDecimal transferAmount) throws IllegalParameterException {
        deposit(account, transferAmount);
    }


    public void payInterest(Account account) throws NotSupportedException{
        if(!account.canPayInterest()){
            throw new NotSupportedException(NOT_SUPPORTED_MSG);
        }

        double interestRate = ((SavingAccount)account).getInterestRate();
        BigDecimal balance = account.getBalance();
        BigDecimal interest = BigDecimal.valueOf(interestRate).multiply(balance);

        account.setBalance(balance.add(interest));
    }

}
