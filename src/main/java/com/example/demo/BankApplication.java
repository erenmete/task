package com.example.demo;

import com.example.demo.model.CheckingAccount;
import com.example.demo.model.Owner;
import com.example.demo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import static java.lang.System.exit;


@SpringBootApplication
public class BankApplication implements CommandLineRunner {

	@Autowired
	AccountService accountService;

	public static void main(String[] args) {
		SpringApplication.run(BankApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {


	}
}
