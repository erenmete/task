package com.example.demo;

import com.example.demo.exceptions.IllegalParameterException;
import com.example.demo.exceptions.NotSupportedException;
import com.example.demo.exceptions.OverdraftException;
import com.example.demo.model.Account;
import com.example.demo.model.CheckingAccount;
import com.example.demo.model.Owner;
import com.example.demo.model.SavingAccount;
import com.example.demo.service.AccountService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;


import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTests {

	@Autowired
	private AccountService accountService;

	@Rule
	public ExpectedException thrown = ExpectedException.none();


	@Test
	public void illegalParameterExceptionTests() {
		Account[] accountArray = createAccounts();
		CheckingAccount ca1 = (CheckingAccount)accountArray[0];

		thrown.expect(IllegalParameterException.class);
		thrown.expectMessage(AccountService.AMOUNT_ERROR_MSG);
		accountService.deposit(ca1, new BigDecimal(	-1000));

	}

	@Test
	public void notSupportedExceptionTest1() {
		Account[] accountArray = createAccounts();
		CheckingAccount ca1 = (CheckingAccount)accountArray[0];

		thrown.expect(NotSupportedException.class);
		thrown.expectMessage(AccountService.NOT_SUPPORTED_MSG);

		accountService.payInterest(ca1);
	}

	@Test
	public void notSupportedExceptionTest2() {
		Account[] accountArray = createAccounts();
		CheckingAccount ca1 = (CheckingAccount)accountArray[0];
		SavingAccount sa1 = (SavingAccount)accountArray[2];

		thrown.expect(NotSupportedException.class);
		thrown.expectMessage(AccountService.NOT_SUPPORTED_MSG);

		accountService.transfer( sa1, ca1, new BigDecimal("2000") );
	}
	@Test
	public void notSupportedExceptionTest3() {
		Account[] accountArray = createAccounts();
		CheckingAccount ca1 = (CheckingAccount)accountArray[0];
		SavingAccount sa1 = (SavingAccount)accountArray[2];

		thrown.expect(NotSupportedException.class);
		thrown.expectMessage(AccountService.NOT_SUPPORTED_MSG);

		accountService.transfer( ca1, sa1, new BigDecimal("2000") );
	}

	@Test
	public void overdraftExceptionTest() {
		Account[] accountArray = createAccounts();
		CheckingAccount ca1 = (CheckingAccount)accountArray[0];

		thrown.expect(OverdraftException.class);
		thrown.expectMessage(AccountService.OVERDRAFT_ERROR_MSG);

		accountService.withdraw( ca1, new BigDecimal("4000") );
	}

	@Test
	public void depositTests() {
		Account[] accountArray = createAccounts();
		CheckingAccount ca1 = (CheckingAccount)accountArray[0];
		CheckingAccount ca2 = (CheckingAccount)accountArray[1];
		SavingAccount sa1 = (SavingAccount) accountArray[2];

		accountService.deposit(ca1, new BigDecimal("1000"));
		assertTrue( ca1.getBalance().compareTo(new BigDecimal("3000")) == 0 );

		accountService.deposit(ca2, new BigDecimal("114.75"));
		assertTrue( ca2.getBalance().compareTo(new BigDecimal("3114.75")) == 0 );

		accountService.deposit(ca2, new BigDecimal("128.44"));
		assertTrue( ca2.getBalance().compareTo(new BigDecimal("3243.19")) == 0 );

		accountService.deposit(sa1, new BigDecimal("2000.15"));
		assertTrue( sa1.getBalance().compareTo(new BigDecimal("6000.15")) == 0 );
	}


	@Test
	public void withdrawTests() {
		Account[] accountArray = createAccounts();
		CheckingAccount ca1 = (CheckingAccount)accountArray[0];
		CheckingAccount ca2 = (CheckingAccount)accountArray[1];
		SavingAccount sa1 = (SavingAccount) accountArray[2];

		accountService.withdraw(ca1, new BigDecimal("1000"));
		assertTrue( ca1.getBalance().compareTo(new BigDecimal("1000")) == 0 );

		accountService.withdraw(ca2, new BigDecimal("1000.19"));
		assertTrue( ca2.getBalance().compareTo(new BigDecimal("1999.81")) == 0 );

		accountService.withdraw(ca2, new BigDecimal("1999.81"));
		assertTrue( ca2.getBalance().compareTo(BigDecimal.ZERO) == 0 );

		accountService.withdraw(ca2, new BigDecimal("1500"));
		assertTrue( ca2.getBalance().compareTo(new BigDecimal("-1500")) == 0 );

		accountService.withdraw(sa1, new BigDecimal("2000.25"));
		assertTrue( sa1.getBalance().compareTo(new BigDecimal("1999.75")) == 0 );
	}

	@Test
	public void transferTests() {
		Account[] accountArray = createAccounts();
		CheckingAccount ca1 = (CheckingAccount)accountArray[0];
		CheckingAccount ca2 = (CheckingAccount)accountArray[1];

		accountService.transfer(ca1, ca2, new BigDecimal("1500"));
		assertTrue( ca1.getBalance().compareTo(new BigDecimal("500")) == 0 );
		assertTrue( ca2.getBalance().compareTo(new BigDecimal("4500")) == 0 );

		accountService.transfer(ca1, ca2, new BigDecimal("1500"));
		assertTrue( ca1.getBalance().compareTo(new BigDecimal("-1000")) == 0 );
		assertTrue( ca2.getBalance().compareTo(new BigDecimal("6000")) == 0 );

		accountService.transfer(ca2, ca1, new BigDecimal("0.99"));
		assertTrue( ca1.getBalance().compareTo(new BigDecimal("-999.01")) == 0 );
		assertTrue( ca2.getBalance().compareTo(new BigDecimal("5999.01")) == 0 );
	}

	@Test
	public void payInterestTests() {
		Account[] accountArray = createAccounts();
		SavingAccount sa1 = (SavingAccount)accountArray[2];
		SavingAccount sa2 = (SavingAccount)accountArray[3];

		accountService.payInterest(sa1);
		assertTrue( sa1.getBalance().compareTo(new BigDecimal("4400")) == 0 );

		accountService.payInterest(sa1);
		assertTrue( sa1.getBalance().compareTo(new BigDecimal("4840")) == 0 );

		accountService.payInterest(sa1);
		assertTrue( sa1.getBalance().compareTo(new BigDecimal("5324")) == 0 );

		accountService.payInterest(sa1);
		assertTrue( sa1.getBalance().compareTo(new BigDecimal("5856.4")) == 0 );

		accountService.payInterest(sa1);
		assertTrue( sa1.getBalance().compareTo(new BigDecimal("6442.04")) == 0 );

		accountService.payInterest(sa2);
		assertTrue( sa2.getBalance().compareTo(new BigDecimal("5250")) == 0 );

		accountService.payInterest(sa2);
		assertTrue( sa2.getBalance().compareTo(new BigDecimal("5512.5")) == 0 );

		accountService.payInterest(sa2);
		assertTrue( sa2.getBalance().compareTo(new BigDecimal("5788.125")) == 0 );

		accountService.payInterest(sa2);
		assertTrue( sa2.getBalance().compareTo(new BigDecimal("6077.53125")) == 0 );
	}

	@Test
	public void mixedTests(){
		Account[] accountArray = createAccounts();
		CheckingAccount ca1 = (CheckingAccount)accountArray[0];
		CheckingAccount ca2 = (CheckingAccount)accountArray[1];
		SavingAccount sa1 = (SavingAccount)accountArray[2];
		SavingAccount sa2 = (SavingAccount)accountArray[3];



	}


	// helper method for creating accounts
	// return[0] and return[1] checking accounts,
	// return[2] and return[3] are saving accounts
	private Account[] createAccounts(){
		Owner owner1 = new Owner("eren", "mete");
		Owner owner2 = new Owner("janie", "doe");
		Owner owner3 = new Owner("john", "doe");

		CheckingAccount account1 = new CheckingAccount(owner3, BigDecimal.valueOf(2000), BigDecimal.valueOf(1000));
		CheckingAccount account2 = new CheckingAccount(owner3, BigDecimal.valueOf(3000), BigDecimal.valueOf(1500));
		SavingAccount account3 = new SavingAccount(owner1, BigDecimal.valueOf(4000), 0.1);
		SavingAccount account4 = new SavingAccount(owner2, BigDecimal.valueOf(5000), 0.05);

		Account[] returnArray = new Account[4];
		returnArray[0] = account1;
		returnArray[1] = account2;
		returnArray[2] = account3;
		returnArray[3] = account4;

		return returnArray;
	}

}
